<?php

$time_milli = (int) round(microtime(true) * 1000);

require_once "JsonReader.php";
require_once "CDM.php";
require_once "Helper.php";

$pointsRaw = JsonReader::read("points.json");
$points = array();

// Converting points JSON into PointTransport objects. 

$points = array();

class Point {

    public $id;
    public $name;
    public $direction;
    public $distance;
    public $lat;
    public $lng;
    public $color;
    public $stop;

}

$lat = isset($_GET['lat'])?$_GET['lat']:0;
$lng = isset($_GET['lng'])?$_GET['lng']:0;
$type = isset($_GET['t'])?$_GET['t']:'plain';

$maxDistance = CDM::oneMeterInDegree() * (isset($_GET['d'])?$_GET['d']:500);

foreach($pointsRaw as $p) {

    /*
    $id, double lat, double lng, boolean stop, int idLine,
                          $lineName, $direction, $color, int sequence,
                          $adjacentPoints, $interchanges 
                          */
    /*
    $points[] = new PointTransport(
        $p->id, $p->lat, $p->lng, $p->st, $p->l, 
        $p->n, $p->d, $p->c, $p->s, $p->a, $interchanges
    );
    */

    $distance = Helper::calculateDistanceLocation($lat, $lng, $p->lat, $p->lng);

    //var_dump($p->d);

    if($distance < $maxDistance)
    {

        $exists = false;
        $update = false;
        foreach($points as $pt) {
            if($p->n == $pt->name and $p->d == $pt->direction) {
                $exists = true;
                if($distance < $pt->distance) {
                    $pt->id = $p->id;
                    $pt->distance = $distance;
                }
                break;
            }
        }

        if(!$exists) {
            $point = new Point();
            $point->id = $p->id;
            $point->name = $p->n;
            $point->direction = $p->d;
            $point->distance = $distance;
            $point->lat = $p->lat;
            $point->lng = $p->lng;
            $point->color = $p->c;
            $point->stop = $p->st;
            $points[] = $point;
        }
    }

}

if($type == 'json') {
    header('content-type:application/json');
    echo json_encode($points);
} else {
    header('content-type:text/plain');
    foreach($points as $p) {
        echo "ID: " . $p->id . " Name: " . $p->name 
        . " Lat: " . $p->lat . " Lng: " . $p->lng
        . " Direction: " . $p->direction 
        . " Distance: " . ($p->distance/CDM::oneMeterInDegree()) . " m\n";
    }
}
