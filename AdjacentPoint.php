<?php

class AdjacentPoint {

    public $transportPoint;
    public $transportCost;

    public function __construct($transportPoint, $transportCost) {
        $this->transportPoint = $transportPoint;
        $this->transportCost = $transportCost;
    }
    
}