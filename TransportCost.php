<?php

class TransportCost {

    public $price;
    public $distance;

    public function __construct($price = PHP_INT_MAX, $distance = PHP_INT_MAX) {
        if(!empty($price) || $price === 0) $this->price = $price;
        if(!empty($distance) || $distance === 0) $this->distance = $distance;
    }

}