<?php

class Dijkstra {

    const PRIORITY_COST = 0;
    const PRIORITY_DISTANCE = 1;
    
    private $iteration = 0;

    public function __construct($graph) {
        $this->graph = $graph;
    }
    
    public function route($source, $destination, $priority = Dijkstra::PRIORITY_COST) {

        $source->setCost(0, 0);

        $settledPoints = array();
        $unsettledPoints = array();
        $unsettledPoints[] = $source;
        
        $this->iteration = 0;
        $currentPoint = null;
        
        while (count($unsettledPoints) != 0) {
            
            if($priority == Dijkstra::PRIORITY_COST)
                $currentPoint = Dijkstra::getLowestPricePoint($unsettledPoints);
            else $currentPoint = Dijkstra::getLowestDistancePoint($unsettledPoints);
            
            //$unsettledPoints->remove($currentPoint);
            $key = array_search($currentPoint, $unsettledPoints);
            if($key !== false) unset($unsettledPoints[$key]);
            
            $adjacencyPairs = $currentPoint->getAdjacentTransportPoints();
            //var_dump($adjacencyPairs);
            //echo count($unsettledPoints) . ":" . count($settledPoints) . "\n";
            foreach ($adjacencyPairs as $adjacencyPair) {
                
                $adjacentPoint = $adjacencyPair->transportPoint;
                $edgeWeight = $adjacencyPair->transportCost;

                if (!Dijkstra::contains($adjacentPoint, $settledPoints)) { //(!$settledPoints.contains(adjacentPoint)) {

                    //if($priority == Dijkstra::PRIORITY_COST)
                    Dijkstra::calculateMinimumPrice($adjacentPoint, $edgeWeight, $currentPoint);
                    //else 
                    Dijkstra::calculateMinimumDistance($adjacentPoint, $edgeWeight, $currentPoint);

                    $key = array_search($adjacentPoint, $unsettledPoints);
                    if($key === false) $unsettledPoints[] = $adjacentPoint;
                }
                
            }
            
            $key = array_search($currentPoint, $settledPoints);
            if($key === false) $settledPoints[] = $currentPoint;
            
            $this->iteration++;
            
        }
        
        //$this->graph->setTransportPoints($settledPoints);
        return $this->graph;
        
    }

    private function contains($needle, $haystack) {
        foreach($haystack as $hay) {
            if($hay->getId() == $needle->getId()) { 
                return true;
            } 
        }
        return false;
    }

    private static function getLowestPricePoint($unsettledPoints) {
        $lowestCostPoint = null;
        $lowestCost = new TransportCost();
        //var_dump($lowestCost);exit;
        foreach ($unsettledPoints as $point) {
            //var_dump($point->getId()); //exit;
            $pointCost = $point->getCost();
            //var_dump($pointCost); //exit;
            if ($pointCost->price < $lowestCost->price) {
                $lowestCost = $pointCost;
                $lowestCostPoint = $point;
            }
        }
        return $lowestCostPoint;
    }

    public function getIteration() {
        return $this->iteration;
    }

    private static function getLowestDistancePoint($unsettledPoints) {
        $lowestDistancePoint = null;
        $lowestDistance = new TransportCost();
        foreach ($unsettledPoints as $point) {
            $pointDistance = $point->getCost();
            if ($pointDistance->distance < $lowestDistance->distance) {
                $lowestDistance = $pointDistance;
                $lowestDistancePoint = $point;
            }
        }
        return $lowestDistancePoint;
    }

    private static function calculateMinimumDistance($evaluationPoint, $edgeWeight, $sourcePoint) {
        $sourceCost = $sourcePoint->getCost();

        //if($evaluationPoint->getId() == "1878") {
        //    echo "-" . $sourcePoint->getId() 
        //    . "-" . $sourcePoint->getCost()->price 
        //    . "-" . $edgeWeight->distance
        //    . "\n";
        //}

        if ($sourceCost->distance + $edgeWeight->distance < $evaluationPoint->getCost()->distance) {
            $evaluationPoint->setDistance($sourceCost->distance + $edgeWeight->distance);
            $evaluationPoint->setPrice($sourceCost->price + $edgeWeight->price);
            $shortestPath = $sourcePoint->getShortestPath();
            $shortestPath[] = $sourcePoint;
            $evaluationPoint->setShortestPath($shortestPath);
            //echo $evaluationPoint->getId() . ":" . $evaluationPoint->getCost()->distance . "\n";
        }

    }

    private static function calculateMinimumPrice($evaluationPoint, $edgeWeight, $sourcePoint) {
        $sourceCost = $sourcePoint->getCost();
        if ($sourceCost->price + $edgeWeight->price < $evaluationPoint->getCost()->price) {
            //echo ($sourceCost->distance) . "+" . $edgeWeight->distance . "=" . ($sourceCost->distance + $edgeWeight->distance) . "\n";
            //$evaluationPoint->getCost()->distance = $sourceCost->distance + $edgeWeight->distance;
            $evaluationPoint->setPrice($sourceCost->price + $edgeWeight->price);
            $cheapestPath = $sourcePoint->getCheapestPath();
            $cheapestPath[] = $sourcePoint;
            $evaluationPoint->setCheapestPath($cheapestPath);
            //echo $evaluationPoint->getId() . ":" . $evaluationPoint->getCost()->distance . "\n";
            //var_dump($evaluationPoint->getCost());
        }

    }
    
}

//0.03309802181609
//0.033081918435308