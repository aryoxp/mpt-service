<?php

class Helper {
    
    public static function calculateDistance($a, $b) {
        $distance = sqrt(pow(($a->lat() - $b->lat()), 2) + pow(($a->lng() - $b->lng()), 2));
        return $distance;
    }

    public static function calculateDistanceLocation($lat1, $lng1, $lat2, $lng2) {
        $distance = sqrt(pow($lat1 - $lat2, 2) + pow($lng1 - $lng2, 2));
        return $distance;
    }
    
    /*
    public static function calculateDistance($a, $lat, $lng) {
        $distance = sqrt(pow(($a.lat() - $lat), 2) + pow(($a.lng() - $lng), 2));
        return $distance;
    }
    */
}
