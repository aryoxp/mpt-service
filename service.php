<?php

$time_milli = (int) round(microtime(true) * 1000);

$type = isset($_GET['t'])?$_GET['t']:'plain';

require_once "Configuration.php";
require_once "CDM.php";
require_once "JsonReader.php";
require_once "TransportCost.php";
require_once "PointTransport.php";
require_once "Helper.php";
require_once "AdjacentPoint.php";
require_once "Dijkstra.php";

$priority = Dijkstra::PRIORITY_DISTANCE;
if(isset($_GET['p']) && $_GET['p'] == "c")
    $priority = Dijkstra::PRIORITY_COST;

$pointsRaw = JsonReader::read("points.json");
$points = array();

// Converting points JSON into PointTransport objects. 
foreach($pointsRaw as $p) {

    /*
    $id, double lat, double lng, boolean stop, int idLine,
                          $lineName, $direction, $color, int sequence,
                          $adjacentPoints, $interchanges */
    $interchanges = ($p->i === null) ? array() : explode(",", $p->i);

    // instantiate PointTransport and add it into collection.
    $points[] = new PointTransport(
        $p->id, $p->lat, $p->lng, $p->st, $p->l, 
        $p->n, $p->d, $p->c, $p->s, $p->a, $interchanges
    );
}

$graph = array();

foreach ($points as $pointTransport) {

    $adjacentPointId = $pointTransport->getAdjacentPointId();
    
    // finding adjacent points
    foreach ($points as $nextPointTransport) {
        if($adjacentPointId !== null && $adjacentPointId == $nextPointTransport->getId()) {
            $distance = Helper::calculateDistance($pointTransport, $nextPointTransport);
            $cost = new TransportCost(0, $distance);
            $pointTransport->addDestination($nextPointTransport, $cost);
        }
    }
    
    // finding interchanges
    $nextInterchangePoints = $pointTransport->getInterchanges();
    if(!empty($nextInterchangePoints)) {
        foreach($points as $nextPointInterchange) {
            if(in_array($nextPointInterchange->getId(), $nextInterchangePoints)) {
                // found! add price cost but no distance cost
                $cost = new TransportCost(CDM::getStandardCost(), 0);
                // add the point in interchange as next destination point
                $pointTransport->addDestination($nextPointInterchange, $cost);
            }
        }
    }

    // add the point into graph
    $graph[] = $pointTransport;
}

$graphtime = (int) round(microtime(true) * 1000) - $time_milli;

$sId = isset($_GET['s'])?$_GET['s']:"1";
$dId = isset($_GET['d'])?$_GET['d']:"10";

$sPoint = null;
$dPoint = null;

// finding starting and destination points
foreach($graph as $point) {
    if($point->getId() == $sId) $sPoint = $point;
    if($point->getId() == $dId) $dPoint = $point;
    if($sPoint != null && $dPoint != null) break;
}

// dijkstra stats initialization
$dijkstratime = 0;

// the resulting path is initialized as null
$path = null;

// RUN the Dijkstra
if($sPoint != null && $dPoint != null) {
    
    $time_milli = (int) round(microtime(true) * 1000);

    $dijkstra = new Dijkstra($graph);
    $dijkstra->route($sPoint, $dPoint, $priority);

    $dijkstratime = (int) round(microtime(true) * 1000) - $time_milli;
    
    if($priority == Dijkstra::PRIORITY_COST) $path = $dPoint->getCheapestPath();
    else $path = $dPoint->getShortestPath();
}

if($type == 'json') {

    header('content-type:application/json');

    $message = new stdClass();
    $message->error = null;
    $message->graphtimems = $graphtime;
    $message->numpoints = count($graph);
    $message->dijkstratimems = $dijkstratime;
    $message->cost = $dPoint->getCost()->price;
    $message->distance = $dPoint->getCost()->distance;
    $message->switch = $dPoint->getCost()->price / CDM::getStandardCost();
    $message->order = array();

    if($sPoint === null) $message->error .= "Invalid starting point.\n";
    if($dPoint === null) $message->error .= "Invalid destination point.\n";

    if($path != null) {

        $lastName = "";
        $lastDirection = "";

        foreach($path as $p) {
            $point = new stdClass();
            $point->id = $p->getId();
            $point->name = $p->getName();
            $point->direction = $p->getDirection();
            $point->color = $p->getColor();
            $point->stop = $p->isStop();
            $point->lat = $p->lat();
            $point->lng = $p->lng();
            $point->price = $p->getCost()->price;
            $point->distance = $p->getCost()->distance;

            $message->path[] = $point;

            if($lastName == $point->name && $lastDirection == $point->direction) continue;
            else {
                $lastName = $point->name;
                $lastDirection = $point->direction;
                $message->order[] = $point->name . ":" . $point->direction;
            }
        }

    } else {
        $message->path = null;
        $message->error .= "Unable to find route to destination.\n";
    }

    echo json_encode($message);
    exit;

} else {

    header('content-type:text/plain');

    echo "OK: Graph of " . count($graph) . " points built in " . $graphtime ." ms.\n"; //exit;
    echo "OK: Dijkstra completed in " . $dijkstratime ." ms.\n\n"; //exit;

    if($path != null) {    
        echo "Resulting route: \n";
        foreach($path as $p) {
            echo $p->getId() . ": " . $p->getName() . " lat: " . $p->lat() . " lng: " . $p->lng() 
            . " cost: Rp. " . $p->getCost()->price . " " . $p->getCost()->distance . "\n";
        }
    } else {
        echo "Unable to find route to destination.";
    }

}