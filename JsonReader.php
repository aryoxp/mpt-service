<?php

class JsonReader {

    public static function read($jsonFile) {

        $rawJson = file_get_contents($jsonFile);
        $points = json_decode($rawJson);
        return $points;

    }

}
