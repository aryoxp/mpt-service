<?php

class PointTransport {

    private $idLine;
    private $lineName;
    private $direction;
    //private $sequence;
    private $adjacentPointId;
    private $adjacentPointInterchangeIds;
    private $id;
    private $lat;
    private $lng;
    private $stop;
    private $color;
    private $price;

    private $transportCost;
    private $cheapestPath; // = new LinkedList<>();
    private $shortestPath; // = new LinkedList<>();

    protected $adjacentTransportPoints = array(); //point, cost

    //"l":"1","n":"AL","d":"O","s":"0","a":"1526","i":null},
    public function __construct ($id, $lat, $lng, $stop, $idLine,
                          $lineName, $direction, $color, $sequence,
                          $adjacentPointId, $interchanges) {
        $this->id = $id;
        $this->lat = $lat;
        $this->lng = $lng;
        $this->stop = $stop;
        $this->idLine = $idLine;
        $this->lineName = $lineName;
        $this->direction = $direction;
        $this->color = $color;
        $this->sequence= $sequence;
        $this->adjacentPointId = $adjacentPointId;
        $this->adjacentPointInterchangeIds = $interchanges;
        $this->price = CDM::getStandardCost();

        $this->transportCost = new TransportCost();
    }

    public function getAdjacentPointId(){
        return $this->adjacentPointId;
    }
    
    public function getId() { return $this->id; }
    public function getName() { return $this->lineName; }
    public function getDirection() { return $this->direction; }
    public function getColor() { return $this->color; }
    public function isStop() { return $this->stop; }
    public function lat() { return $this->lat; }
    public function lng() { return $this->lng; }

    public function addDestination($destination, $cost) {
        $key = array_search($destination, $this->adjacentTransportPoints);
        if($key === false)
            $this->adjacentTransportPoints[] = new AdjacentPoint($destination, $cost);
    }
    public function getInterchanges() { return $this->adjacentPointInterchangeIds; }
    public function getAdjacentTransportPoints() { return $this->adjacentTransportPoints; }
    public function getCost() { return $this->transportCost; }
    public function setCost($price, $distance) { 
        $this->transportCost->price = $price;
        $this->transportCost->distance = $distance; 
    }  

    public function setDistance($distance) { $this->transportCost->distance = $distance; }
    public function setPrice($price) { $this->transportCost->price = $price; }
    public function getCheapestPath() { return $this->cheapestPath; }
    public function getShortestPath() { return $this->shortestPath; }
    public function setCheapestPath($cheapestPath) { $this->cheapestPath = $cheapestPath; }
    public function setShortestPath($shortestPath) { $this->shortestPath = $shortestPath; }

    /*
    public function lat() { return $this->lat; }
    public function lng() { return $this->lng; }
    public function isStop() { return $this->stop; }
    public function getId() { return $this->id; }
    public function getIdLine() { return $this->idLine; }
    // public function getColor() { return Color.parseColor($this->color); }
    public function getLineName() { return $this->lineName; }
    public function getPrice() { return $this->price; }
    
    public function getInterchanges() { // return String[]
        if ($this->adjacentPointInterchangeIds != null)
            return split(", ", $this->adjacentPointInterchangeIds);
        else return null;
    }

    private List<PointTransport> cheapestPath; // = new LinkedList<>();
    private List<PointTransport> shortestPath; // = new LinkedList<>();
    private TransportCost transportCost = new TransportCost();

    public void addDestination(PointTransport destination, TransportCost cost) {
        $this->adjacentTransportPoints.put(destination, cost);
    }
    public void clearDestination() {
        $this->adjacentTransportPoints.clear();
        $this->adjacentTransportPoints = new HashMap<>();
    }
    public Map<PointTransport, TransportCost> getAdjacentTransportPoints() { return $this->adjacentTransportPoints; }

    public void setCost(TransportCost transportCost) { $this->transportCost = transportCost; }
    public TransportCost getCost() { return $this->transportCost; }

    public List<PointTransport> getCheapestPath() { return $this->cheapestPath; }
    public List<PointTransport> getShortestPath() { return $this->shortestPath; }
    public void setCheapestPath(LinkedList<PointTransport> cheapestPath) { $this->cheapestPath = cheapestPath; }
    public void setShortestPath(LinkedList<PointTransport> shortestPath) { $this->shortestPath = shortestPath; }

    public function getCheapestPathCost() {
        $cost = 0D;
        $currentLineId = 0;
        for (PointTransport p: $this->cheapestPath) {
            if(cost == 0 && currentLineId == 0) {
                cost += p.price;
                currentLineId = p.getIdLine();
            }
            if(currentLineId != p.getIdLine()) {
                currentLineId = p.getIdLine();
                cost += p.price;
            }
        }
        return cost;
    }

    public void setCost($price, $distance) {
        $this->transportCost.distance = distance;
        $this->transportCost.price = price;
    }

    public void setDistance($distance) {
        $this->transportCost.distance = distance;
    }
    public void setPrice($price) {
        $this->transportCost.price = price;
    }

    public function getDirection() {
        if($this->direction != null && $this->direction.equals("O")) return "Outbound";
        if($this->direction != null && $this->direction.equals("I")) return "Inbound";
        return direction;
    }

    public static class TransportCost {
        public function price = Double.MAX_VALUE;
        public function distance = Double.MAX_VALUE;

        public TransportCost(){}

        public TransportCost($price, $distance) {
            $this->price = price;
            $this->distance = distance;
        }
    }
    */

}